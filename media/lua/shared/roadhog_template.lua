require "Scripting/MFManager"

local roadhog_default = {
    name = "roadhog_default",
    script = "Base.MaleFolk",
    skin = {0.98,0.79,0.49},
    haircut = { "Spike", {0.20392157137393951,0.1921568661928177,0.1882352977991104} },
    beard = { "Full", {0.20392157137393951,0.1921568661928177,0.1882352977991104} },
    clothes = {
        { "Tshirt_PoloTINT", {0,0,0} },
        "Gloves_LeatherGloves" ,
        "Trousers_Denim" ,
        "Jacket_LeatherBarrelDogs",
        "Shoes_BlackBoots",
    }
};

table.insert(MFManager.templates, roadhog_default)