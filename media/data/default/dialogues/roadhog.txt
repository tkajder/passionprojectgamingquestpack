#is_quest roadhog_q1|locked
    #m Roadhog|roadhog.png|"Hey there, will you get an empty gas can for me?"
    #choice
        roadhog_q1_yes|Yes
        roadhog_q1_no|No
    
    *roadhog_q1_yes
    #quest_unlock roadhog_q1
    #m You decide to help Roadhog get an empty gas can.
    #has_item Base.EmptyPetrolCan,1
        #m Roadhog|roadhog.png|"Oh, you already have one. Hand it over!"
        #jump roadhog_q1_deliver
    #m Roadhog|roadhog.png|"If you don't have one you can find one at the garage behind you."
    #task_unlock roadhog_q1|t2
    #exit
    *roadhog_q1_no
    #m Roadhog|roadhog.png|"That's a shame.
    #exit
#is_quest roadhog_q1|unlocked,uncompleted
    #m Roadhog|roadhog.png|"Got the empty gas can yet?"
    *roadhog_q1_deliver
    #deliver roadhog_q1|t5
        #m Roadhog|roadhog.png|"Awesome, thanks for the gas can!"
        #exit
    #m Roadhog|roadhog.png|"Suppose not."
    #exit

#m Roadhog|roadhog.png|”Glad to see you alive, my friend.”
#exit